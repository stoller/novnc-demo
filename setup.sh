#!/bin/sh

VNCDIR="/users/$USER/.vnc"
VNCPSWD="$VNCDIR/passwd"

#
# David provided this code, from the openstack profile.
#
OURDIR=$VNCDIR
DPKGOPTS=''
APTGETINSTALLOPTS='-y'
APTGETINSTALL="apt-get $DPKGOPTS install $APTGETINSTALLOPTS"

#dpkg -s python-cryptography >/dev/null 2>&1
#if [ ! $? -eq 0 ]; then
#    apt-get $DPKGOPTS install $APTGETINSTALLOPTS python-cryptography
#    # Keep trying again with updated cache forever;
#    # we must have this package.
#    success=$?
#    while [ ! $success -eq 0 ]; do
#	apt-get update
#	apt-get $DPKGOPTS install $APTGETINSTALLOPTS python-cryptography
#	success=$?
#    done
#fi

# The manifest holds the encrypted randomly generated password.
if [ ! -e $OURDIR/manifest.xml ]; then
    geni-get manifest > $OURDIR/manifest.xml
    cat $OURDIR/manifest.xml | grep -q emulab:password
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get manifest failed or no password block!"
	exit 1
    fi
fi

# Geni key decrypts the password.
if [ ! -e $OURDIR/geni.key ]; then
    geni-get key > $OURDIR/geni.key
    cat $OURDIR/geni.key | grep -q END\ .\*\PRIVATE\ KEY
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
fi

# Suck out the key from the manifest
if [ ! -e $OURDIR/encrypted_admin_pass ]; then
    cat $OURDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:password[^>]*>([^<]+)<\/[^:]+:password>.+/igs) { print $1; }' > $OURDIR/encrypted_admin_pass
fi

# And descrypt to get the password in plain text.
if [ ! -e $OURDIR/decrypted_admin_pass -a -s $OURDIR/encrypted_admin_pass ]; then
    openssl smime -decrypt -inform PEM -inkey $OURDIR/geni.key -in $OURDIR/encrypted_admin_pass -out $OURDIR/decrypted_admin_pass
    if [ $? -ne 0 ]; then
	echo "ERROR: openssl decrypt failed!"
	exit 1
    fi
fi

# And setup a vnc passwd. 
if [ ! -e $VNCDIR/passwd ]; then
    cat $OURDIR/decrypted_admin_pass | vncpasswd -f > $VNCDIR/passwd
    if [ $? -ne 0 ]; then
	echo "ERROR: vncpasswd failed!"
	exit 1
    fi
    chmod 600 $VNCDIR/passwd
fi

# And start the vncserver.
#$VNCDIR/vncserver.open
exit 0
