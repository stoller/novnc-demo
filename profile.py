"""Setup VNC for a novnc demo

Instructions:
Wait for the experiment to start, your VNC passwd is `{password-vncpswd}`
"""

#
# This is a git repo with my dot files and junk I like.
#
URL = "https://gitlab.flux.utah.edu/stoller/dots/-/raw/master/dots.tar.gz"

#
# Download tarfile to /tmp
#
GETTAR = "wget -O /tmp/dots.tar.gz " + URL

#
# Not so obvious tar command.
#
UNTAR = "sudo -u stoller tar zxf /tmp/dots.tar.gz -p -C /users/stoller --strip-components=1"

#
# Repo package install script.
#
INSTALL = "sudo /bin/bash /local/repository/install.sh"

#
# And the setup script to start VNC with the randomly generated password.
#
SETUP = "/local/repository/setup.sh"

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Import IG extensions
import geni.rspec.igext as ig

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()
 
# Add a raw PC to the request.
node = request.RawPC("node")
node.hardware_type = "d430"

# Create a per-experiment password
password = ig.Password("vncpswd")
request.addResource(password)

# Install from gitlab via the tar link.
node.addService(rspec.Execute(shell="bash", command=GETTAR))
node.addService(rspec.Execute(shell="bash", command=UNTAR))
node.addService(rspec.Execute(shell="sh", command=INSTALL))
node.addService(rspec.Execute(shell="sh", command=SETUP))
        
portal.context.printRequestRSpec()

