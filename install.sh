#!/bin/sh

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get -y install --no-install-recommends socat tigervnc-standalone-server tigervnc-common autocutsel fvwm x11-xserver-utils x11-apps xfonts-base xfonts-100dpi xfonts-75dpi
if [ $? -ne 0 ]; then
    echo 'apt-get install support failed'
    exit 1
fi

exit 0
